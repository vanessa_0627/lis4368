> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Vanessa Sanabria

### Assignment 3 Requirements:

*Deliverables:*

1. Entity Relation Diagram (ERD)
2. Include data (at  least 10 records each table)
3. Forward engineer diagram to course repo

#### README.md file should include the following items:

* Screenshot of ERD
* Table Data of ERD
* Screenshots of Skillsets 4-6
* MWB link
* SQL Script link


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
>

#### Assignment Screenshots and links:

*Screenshot of A3 ERD:

![A3 ERD](img/erd.png "ERD based upon A3 requirements")

*Screenshot of A3 Pet Store Inserts:

![A3 Pet Store table](img/petstore.png "inserts based upon A3 requirements")

*Screenshot of A3 Pet Table Inserts:

![A3 pet table](img/pet.png "inserts based upon A3 requirements")

*Screenshot of A3 Customer Table Inserts:

![A3 customer table](img/customer.png "inserts based upon A3 requirements")

*Screenshot of Skillset 4:

![Skillset 4](img/q4.png "Skillset 4")

*Screenshot of Skillset 5:

![Skillset 5](img/q5.png "Skillset 5")

*Screenshot of Skillset 6:

![Skillset 6](img/q6.png "Skillset 6")


#### Links:

*A3 docs: a3.mwb and a3.sql*

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")
