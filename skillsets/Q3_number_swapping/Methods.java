import java.util.*; 

public class Methods
{
public static void getRequirements()
{
    System.out.println("Developer: Vanessa Sanabria");
        System.out.println("Program requirements:");
        System.out.println("1. Program swaps two user-entered floating-point values.");
        System.out.println("Create at least two user-defined methods: getRequirements() and numberSwap().");
        System.out.println("Must perform data validation numbers must be floats.");
    
}
//ponyvalue-returning method accepts int array arg (static requires no object)
public static void numberSwap()
{
    Scanner sc = new Scanner(System.in);
    float num1=0.0f;
    float num2=0.0f;
    float temp=0.0f;

//prompt user
//hasNextFloat(); returns true if next token in scanner's inpit can be interpreted as float value 
System.out.print("Enter num1: ");
while (!sc.hasNextFloat())
{
    System.out.println("Invalid input!\n");
    sc.next(); //important! if omitted, will go into infinite loop on invalid input!
    System.out.print("Please try again. Enter num1:");
}
num1 = sc.nextFloat(); 

System.out.print("\nEnter num2");
while (!sc.hasNextFloat())
{
    System.out.println("Invalid input\n");
    sc.next();
    System.out.print("Please try again. Enter num2:");
}
num2 = sc.nextFloat(); //valid input 

System.out.println("\nBefore swap:");
System.out.println("num1 = " + num1);
System.out.println("num2 = " + num2);

//value of num1 assigned to temp
temp = num1;

//value of num2 assigned to num 1
num1=num2;

//value of temp (containing inital value of num1) assigned to num2

num2 = temp;

    System.out.println("\nAfter swap:");
    System.out.println("num1 = " + num1);
    System.out.println("num2 = " + num2);

    sc.close(); //close scanner
}
}