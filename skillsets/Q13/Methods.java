import java.util.*; 

public class Methods
{
//Create Method without returning any value (without object)
public static void getRequirements()
{
    //display operational messages
    System.out.println("Inside vehicle default constructor.");
    System.out.println();
    System.out.println("Make = My Make");
    System.out.println("Model = My Model");
    System.out.println("Year = 2000");
    System.out.println();
    System.out.println("Inside vehicle constructor with parameters");
    System.out.println();
    System.out.println("Make: Toyota");
    System.out.println("Model: Camry Hybrid");
    System.out.println("Year: 2020");
    System.out.println();
    System.out.println("Inside vehicle default constructor");
    System.out.println();
    System.out.println("Inside car default constructor");
    System.out.println("Make = My Make");
    System.out.println("Model = My Model");
    System.out.println("Year = 2000");
    System.out.println("Speed = 100.0");
    System.out.println();
    System.out.println("Make: Toyota");
    System.out.println("Model: Camry Hybrid");
    System.out.println("Year: 2020");
    System.out.println("Speed = 140.0");
    System.out.println();
    System.out.println("Inside vehicle constructor with parameters.");
    System.out.println();
    System.out.println("Inside car constructor with parameters.");
    System.out.println("Make: Toyota");
    System.out.println("Model: Camry Hybrid");
    System.out.println("Year: 2020");
    System.out.println("Speed = 140.0");
}
}




