import java.util.Scanner;

public class Methods
{


    public static void getRequirements()
    {
        
        System.out.println("Developer: Vanessa Sanabria");
        System.out.println("Program determines whether user-entered value is a vowel, consonant, special character or integer.");
        System.out.println("Program determines character\'s ASCII value");
        
        System.out.println("\nReferences:\n"
                    + "ASCII Background: http://en.wikipedia.org/wiki/ASCII\n"
                    + "ASCII Character Table: https://www.ascii-code.com/\n"
                    + "Lookup Tables: https://www.lookuptables.com/");

        System.out.println();  // print blank line
    }
    
    public static void determineChar()
    {
        //initialize variables
        char ch = ' ';
        char chTest = ' ';
        Scanner sc = new Scanner (System.in);

        System.out.println("Please enter character: ");
        
        
        
        
        ch = sc.nextLine().charAt(0);
        chTest = Character.toLowerCase(ch);       
      
      


      if((chTest == 'a' || chTest == 'e' || chTest == 'i'|| chTest == 'o'|| chTest == 'u'|| chTest == 'y'))
      {
        System.out.println(ch +" is a vowel. ASCII value is: " + (int)ch);
      }
        
       else if (ch >= '0' && ch <= '9')
       {
        System.out.println(ch +" is an integer. ASCII value is: " + (int)ch);
      }
       
       else if((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z'))
        {
        System.out.println(ch + " is a consonant. ASCII value is: "  + (int)ch);
      }
       
       else
        {
        System.out.println(ch +" is a special character. ASCII value is: " + (int)ch);
      }

       sc.close();
    }
}

       
       
       