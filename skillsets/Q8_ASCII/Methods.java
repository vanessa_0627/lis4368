import java.util.Scanner;

public class Methods
{
    public static void getRequirements()
    {
        System.out.println("Developer: Vanessa Sanabria");
        System.out.println("Program Requirements:");
        System.out.println("\t1. Write an  application that displays integer equivalents of uppercase letters.");
        System.out.println("\t2. Display chracter equivalents of ASCII values 48-122.");
        System.out.println("\t3. Display the character equivalent of ASCII value user input.");
        System.out.println("\tNote: Compare answers to http://www.asciitable.com/");

        System.out.println(""); //print blank line
        System.out.println("Printing characters A-Z as ASCII values:");
        System.out.println("Character A has ascii value 65");
        System.out.println("Character B has ascii value 66");
        System.out.println("Character C has ascii value 67");
        System.out.println("Character D has ascii value 68");
        System.out.println("Character E has ascii value 69");
        System.out.println("Character F has ascii value 70");
        System.out.println("Character G has ascii value 71");
        System.out.println("Character H has ascii value 72");
        System.out.println("Character I has ascii value 73");
        System.out.println("Character J has ascii value 74");
        System.out.println("Character K has ascii value 75");
        System.out.println("Character L has ascii value 76");
        System.out.println("Character M has ascii value 77");
        System.out.println("Character N has ascii value 78");
        System.out.println("Character O has ascii value 79");
        System.out.println("Character P has ascii value 80");
        System.out.println("Character Q has ascii value 81");
        System.out.println("Character R has ascii value 82");
        System.out.println("Character S has ascii value 83");
        System.out.println("Character T has ascii value 84");
        System.out.println("Character U has ascii value 85");
        System.out.println("Character V has ascii value 86");
        System.out.println("Character W has ascii value 87");
        System.out.println("Character X has ascii value 88");
        System.out.println("Character Y has ascii value 89");
        System.out.println("Character Z has ascii value 90");
        System.out.println("Printing ASCII values 48-122 as characters:");
        System.out.println("ASCII value 48 has character value 0");
        System.out.println("ASCII value 49 has character value 1");
        System.out.println("ASCII value 50 has character value 2");
        System.out.println("ASCII value 51 has character value 3");
        System.out.println("ASCII value 52 has character value 4");
        System.out.println("ASCII value 53 has character value 5");
        System.out.println("ASCII value 54 has character value 6");
        System.out.println("ASCII value 55 has character value 7");
        System.out.println("ASCII value 56 has character value 8");
        System.out.println("ASCII value 57 has character value 9");
        System.out.println("ASCII value 58 has character value :");
        System.out.println("ASCII value 59 has character value ;");
        System.out.println("ASCII value 60 has character value <");
        System.out.println("ASCII value 61 has character value =");
        System.out.println("ASCII value 62 has character value >");
        System.out.println("ASCII value 63 has character value ?");
        System.out.println("ASCII value 64 has character value @");
        System.out.println("ASCII value 65 has character value A");
        System.out.println("ASCII value 66 has character value B");
        System.out.println("ASCII value 67 has character value C");
        System.out.println("ASCII value 68 has character value D");
        System.out.println("ASCII value 69 has character value E");
        System.out.println("ASCII value 70 has character value F");
        System.out.println("ASCII value 71 has character value G");
        System.out.println("ASCII value 72 has character value H");
        System.out.println("ASCII value 73 has character value I");
        System.out.println("ASCII value 74 has character value J");
        System.out.println("ASCII value 75 has character value K");
        System.out.println("ASCII value 76 has character value L");
        System.out.println("ASCII value 77 has character value M");
        System.out.println("ASCII value 78 has character value N");
        System.out.println("ASCII value 79 has character value O");
        System.out.println("ASCII value 80 has character value P");
        System.out.println("ASCII value 81 has character value Q");
        System.out.println("ASCII value 82 has character value R");
        System.out.println("ASCII value 83 has character value S");
        System.out.println("ASCII value 84 has character value T");
        System.out.println("ASCII value 85 has character value U");
        System.out.println("ASCII value 86 has character value V");
        System.out.println("ASCII value 87 has character value W");
        System.out.println("ASCII value 88 has character value X");
        System.out.println("ASCII value 89 has character value Y");
        System.out.println("ASCII value 90 has character value Z");
        System.out.println("ASCII value 91 has character value [");
        System.out.println("ASCII value 92 has character value '\'");
        System.out.println("ASCII value 93 has character value ]");
        System.out.println("ASCII value 94 has character value ^");
        System.out.println("ASCII value 95 has character value _");
        System.out.println("ASCII value 96 has character value `");
        System.out.println("ASCII value 97 has character value a");
        System.out.println("ASCII value 98 has character value b");
        System.out.println("ASCII value 99 has character value c");
        System.out.println("ASCII value 100 has character value d");
        System.out.println("ASCII value 100 has character value e");
        System.out.println("ASCII value 100 has character value f");
        System.out.println("ASCII value 100 has character value g");
        System.out.println("ASCII value 100 has character value h");
        System.out.println("ASCII value 100 has character value i");
        System.out.println("ASCII value 100 has character value j");
        System.out.println("ASCII value 100 has character value k");
        System.out.println("ASCII value 100 has character value l");
        System.out.println("ASCII value 100 has character value m");
        System.out.println("ASCII value 100 has character value n");
        System.out.println("ASCII value 100 has character value o");
        System.out.println("ASCII value 100 has character value p");
        System.out.println("ASCII value 100 has character value q");
        System.out.println("ASCII value 100 has character value r");
        System.out.println("ASCII value 100 has character value s");
        System.out.println("ASCII value 100 has character value t");
        System.out.println("ASCII value 100 has character value u");
        System.out.println("ASCII value 100 has character value v");
        System.out.println("ASCII value 100 has character value w");
        System.out.println("ASCII value 100 has character value x");
        System.out.println("ASCII value 100 has character value y");
        System.out.println("ASCII value 100 has character value z");

    }
        
public static void getAscii()
{

//indialize variables
int num = 0;
boolean isValidNum = false;
Scanner sc = new Scanner (System.in);

System.out.println("Printing characters A-Z as ASCII values:");
for(char character = 'A'; character <= 'Z';character++)
{
    System.out.printf("Character %c has ASCII value %d\n", character, ((int)character));
}

//Or, typecasting:
System. out. println("\nPrinting ASCII values 48-122 as characters:");
for( num=48; num<=122; num++)
{
    System.out.printf("ASCII value %d has character value %c\n", num, ((char)num));
}

//allow user input
System.out.println("\nAllowing user ASCII value input:");

while (isValidNum == false)
{
//check num double
System.out.print("Please enter ASCII value (32 - 127): ");
if (sc.hasNextInt())
{
    num = sc.nextInt();
    isValidNum = true;  
}
else 
{
    System.out.println( "Invalid Integer--ASCII value must be a number. \n");
}
sc.nextLine(); // discard any other data entered on line

// num data true, check num range
if (isValidNum == true && num < 32 || num > 127)
{
System.out. println("ASCII value must be >= 32 and <= 127.\n");
isValidNum = false;
}
if (isValidNum == true);
{
System.out.println();

// display result
System.out.printf("ASCII value %d has character value %c\n", num, ((char)num));

}
}
sc.close(); //close scanner
    }
}