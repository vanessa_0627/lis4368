> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Vanessa Sanabria

### Project 1 Requirements
*Sub-Heading:*

1. Modify jQuery validation 

#### README.md file should include the following items:

* Screenshots of passed validations
* Screenshots of failed validations

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Project 1 Screenshots:

*Screenshot of passed validation*:

![Passed Validation](img/passed_p1.png)

*Screenshot of failed validation*:

![Failed Validation](img/failed_p1.png)

#### Skillset Screenshots:

*Skillset 7*:

![skillset 7](img/Q7.png)

*Skillset 8*:

![skillset 8](img/Q8_1.png)
![skillset 8](img/Q8_2.png)
![skillset 8](img/Q8_3.png)
![skillset 8](img/Q8_4.png)
![skillset 8](img/Q8_5.png)

*Skillset 9*:

![skillset 9](img/Q9.png)



