> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Vanessa Sanabria

### Assignment 5 Requirements:

*Sub-Heading:*

1. Screenshot of confirmed customer form 
2. screenshot of thank you page  
3. Updated database 
4. Skillsets 13-15

#### README.md file should include the following items:

* Customer form confirmation
* Thank you page
* New row in database table 
* Skillsets 13-15

#### Assignment Screenshots:

*Screenshot of customer confirmation*:

![Confirmation Screenshot](img/confirmation.png)

*Screenshot of updated database table*:

![table screenshot](img/sql.png)

*Skillset 13*:

![Skillset 13](img/Q13.png)

*Skillset 14*:

![Skillset 14](img/Q14.png)

*Skillset 15*:

![Skillset 15](img/Q15.png)



