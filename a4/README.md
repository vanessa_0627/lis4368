> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Vanessa Sanabria

### Assignment 4 Requirements:

*Deliverables:*

1. Server-Side Validation error 
2. Server-Side Validation Thank you Page 

#### README.md file should include the following items:

* Screenshot of failed validation
* Screenshot of passed validation 
* Screenshots of Skillsets 10-12


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
>

#### Assignment Screenshots and links:

*Screenshot of A4 failed validation:

![A4](img/a4.png "server error")

*Screenshot of A4 passed validation:

![A4](img/valid_a4.png "thank you page")



*Screenshot of Skillset 10:

![Skillset 10](img/10.png "Skillset 10")

*Screenshot of Skillset 11:

![Skillset 11](img/11.png "Skillset 11")

*Screenshot of Skillset 12:

![Skillset 12](img/12.png "Skillset 12")



