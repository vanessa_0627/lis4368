> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS  4368

## Vanessa Sanabria

### Project 2 Requirements:

#### README.md file should include the following items:

* Screenshots of working form 

#### Project 2 Screenshots:

*Valid User Form Entry (customerform.jsp)*:

![valid user form entry](img/validForm.png)

*Screenshot of Passed Validation*:

![Passed Validation](img/passedValidation.png)

*Display Data (customers.jsp)*:

![Data with added input](img/displayData.png)

*Modify Form  (modify.jsp)*:

![modifying the form](img/modifyForm.png)

*Modified Data (customers.jsp)*:

![Data with updated input](img/modifyData.png)

*Delete Warning (customers.jsp)*:

![Delete Warning](img/delete.png)

*Associated Database Changes (Select, Insert, Update, Delete)*:

![Database changes](img/select.png)

![Insert](img/insert.png)

![Update](img/update.png)

