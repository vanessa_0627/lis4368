> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# Course LIS4368  -  Advanced Web Applications 

## Vanessa Sanabria

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials
    (Bitbucketstationlocations and myteamquotes)
    - provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Install MYSQL Workbench 
    - Create and compile java servlet 
    - Write a database servlet 
    - Provide screenshots of running servlets
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create a Entity Relationship Diagram 
    - Insert random data in tables 
    - Foward Engineer SQL Script 
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create a server side validation
    - Compile Sevlets 
    - Skillsets 10-12
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Use Server side validation to add data in a table
    - Update SQL table with data  
    - Skillsets 13-15 
6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Working form validation
    - Research form validation code 
    - editing jQuery validation
    - Skillsets 7-9
7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Use MVC framework
    - Provide create, read, update, and delete (CRUD) functionality 
    - Prevent SQL Injection
    - Provide screenshots of functioning form 