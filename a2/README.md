> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Vanessa Sanabria

### Assignment 2 Requirements:

*Required to:*

1. Install MYSQL Workbench
2. Create and compile java servlets
3. Create and compile database servlets 
4. Complete and compile skillsets 1-3

#### README.md file should include the following items:

* Screenshots of java servlet running
* Screenshots of database servlets running 
* Skillsets 1-3
* 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Tomcat running http://localhost:9999/hello*:

![Homepage Screenshot](img/hello.png)

*Screenshot of Running Java Servlet*:

![Hello World Screenshot](img/sayhello.png)

*Screenshot of Querybook Database Servlet*:

![Database Servlet Screenshot](img/querybook.png)

*Screenshot of Querybook Database Results*: 

![Database Results Servlet](img/queryresults.png)

*Screenshot of SkillSet 1*: 

![Skillset one](img/Q1.png)

*Screenshot of SkillSet 2*: 

![Skillset two](img/Q2.png)

*Screenshot of SkillSet 3*: 

![Skillset three](img/Q3.png)

#### Tutorial Links:

*Tutorial: Java Servlet Tutorial:*
[A2 Apache Tomcat Java Servlet Tutorial Link](https://personal.ntu.edu.sg/ehchua/programming/howto/Tomcat_HowTo.html "Java Servlet Tutorial")
