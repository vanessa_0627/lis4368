> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Course LIS4368

## Vanessa Sanabria

### Assignment 1 Requirements:

*Three parts:*

1. Dsitributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development installations
3. Chapter Questions (Ch 1-4)

#### README.md file should include the following items:

* Screenshot of java Hello (#1 above)
* Screenshot of running http://localhost:9999 (#2 above, step 4(b) in tutorial)
* git commands with short descriptions
* Bitbucket repo links: a) this assignment and b)the completed tutorial above (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Initializes a new Git repository.
2. git status - This command provides all the status of the current working tree.
3. git add - This command adds file contents to the index.
4. git commit - This command records all the changes that has been made to the repository.
5. git push - Your commands would be uploaded to a remote repository.
6. git pull - Will fetch the most recent updates from the remote repository and will merge them with your local repository.
7. git clone - A command that will clone a remote repository into a new directory on your local drive.

#### Assignment Screenshots:

*Screenshot of Tomcat running http://localhost*:

![Tomcat Installation Screenshot](img/tomcat.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

